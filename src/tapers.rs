use std::f64::consts::PI;

extern crate lapack;
extern crate lapack_src;

pub fn sinetapers(sz: usize, ntaper: usize) -> Vec<f64> {
    let np1f = (sz + 1) as f64;
    let cnst = (2.0 / np1f).sqrt();
    let tapers: Vec<f64> = (1..(ntaper + 1))
        .map(|k: usize| {
            let kf = k as f64;
            (1..(sz + 1)).map(move |j| {
                let jf = j as f64;
                cnst * (PI * kf * jf / np1f).sin()
            })
        })
        .flatten()
        .collect();
    tapers
}

pub fn slepians(sz: usize, nw: f64, ntaper: usize) -> Vec<f64> {
    // Create the two arrays that define the symmetric tri-diagonal matrix:
    let nf = sz as f64;
    let diag: Vec<f64> = (1..(sz + 1))
        .map(|j| {
            let jf = j as f64; // There must be a better way to do this....
            (2.0 * PI * nw / nf).cos() * (0.5 * (nf - 1.0) - (jf - 1.0)).powi(2)
        })
        .collect();
    let odiag: Vec<f64> = (1..sz) // I just cannot figure out float ranges...
        .map(|j| (0.5 * (j as f64) * (nf - (j as f64))))
        .collect();
    // Create other mutable options to pass to LAPACK:
    let mut info = 0 as i32;
    let mut nv = ntaper as i32;
    let mut nsplit = vec![0 as i32; 1];
    let mut vals = vec![0.0; ntaper];
    let mut iblock = vec![0 as i32; sz];
    let mut isplit = vec![0 as i32; sz];
    let mut work = vec![0.0; 5 * sz];
    let mut iwork = vec![0 as i32; 3 * sz];
    // Get the eigenvalues using dstebz, check info:
    unsafe {
        lapack::dstebz(
            b'I',
            b'E',
            sz as i32,
            0.0,
            0.0,
            (sz - ntaper + 1) as i32,
            sz as i32,
            0.0,
            &diag,
            &odiag,
            &mut nv,
            &mut nsplit,
            &mut vals,
            &mut iblock,
            &mut isplit,
            &mut work,
            &mut iwork,
            &mut info,
        );
    }
    assert!(
        info == 0,
        "Eigenvalue computation for slepian functions has failed."
    );
    // Get the corresponding eigenvectors using dstein, check info:
    let mut vecs = vec![0.0; ntaper * sz];
    let mut ifail = vec![0 as i32; ntaper];
    unsafe {
        lapack::dstein(
            sz as i32,
            &diag,
            &odiag,
            ntaper as i32,
            &vals,
            &iblock,
            &isplit,
            &mut vecs,
            sz as i32,
            &mut work,
            &mut iwork,
            &mut ifail,
            &mut info,
        );
    }
    assert!(
        info == 0,
        "Eigenvector computation for slepian functions has failed."
    );
    vecs
}
