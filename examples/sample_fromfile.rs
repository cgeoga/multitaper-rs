extern crate multitaper;

use std::io::{BufRead, BufReader, Write};
use std::{env, fs};

// This is obviously not the most performant (and probably not the most
// idiomatic) way to read in or write out files. But it's fast enough for an
// example and proof of concept.
fn main() {
    // Parse the given args (filepath, output filepath, NW, K, padlen):
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let outname = &args[2];
    let nw = args[3]
        .parse::<f64>()
        .expect("Failed to read NW parameter.");
    let k = args[4]
        .parse::<usize>()
        .expect("Failed to read K parameter.");
    let padlen = args[5]
        .parse::<usize>()
        .expect("Failed to read pad length parameter.");

    // Open a BufReader for the file:
    let fil = fs::File::open(filename).expect("Failed to read file.");
    let buf = BufReader::new(fil);
    let mut lines = buf.lines();

    // Figure out how many columns, allocate accordingly:
    let mut colvecs: Vec<Vec<f64>> = lines
        .next()
        .expect("Input file has no lines.")
        .expect("Failed to read line in input file.")
        .split(|s| s == '\t' || s == ' ' || s == ',') // common delimiters
        .map(|j| vec![j.trim().parse::<f64>().expect("Failed to parse integer.")])
        .collect();

    // Parse each column of the file into a vector:
    for line in lines {
        for (newj, colj) in line
            .expect("Failed to read line in input file.")
            .split(|s| s == '\t' || s == ' ' || s == ',') // common delimiters
            .map(|j| j.trim().parse::<f64>().expect("Failed to parse integer."))
            .zip(colvecs.iter_mut())
        {
            colj.push(newj);
        }
    }

    // Compute the multitaper estimator:
    let specs: Vec<Vec<f64>> = colvecs
        .iter()
        .map(|cj| multitaper::multitaper_dpss(cj, nw, k, padlen))
        .collect();

    // Try to create the output file:
    let mut out = fs::OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(outname)
        .expect("Failed to open output file.");

    // Write to the output file in the same column-wise format as the input:
    for j in 0..specs[0].len() {
        for (k, skj) in specs.iter().map(|colk| colk[j]).enumerate() {
            if k < colvecs.len() - 1 {
                write!(out, "{}\t", skj.to_string()).expect("Failed to write to output.");
            } else {
                writeln!(out, "{}", skj.to_string()).expect("Failed to write to output.");
            }
        }
    }
}
