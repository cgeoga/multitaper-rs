# multitaper-rs

A very basic multitaper spectral density estimator in rust. There really isn't
much to this code as of now---it does not provide any of the nontrivial features
like adaptive weighting or quadratic bias correction. I've really just used it
to explore the Rust language a it.

It does provide simple multitaper spectral density estimators with either sine
or dpss tapers, and it has an internal function `multitaper` that is largely
non-allocating, which may make it useful for many repeated calls (like, for
example, in computing a spectrogram). Every estimate is centered(/de-meaned) and
flat weighting is used. The source functions `multitaper_dpss` and
`multitaper_sine` show examples of doing the necessary allocations and then
calling `multitaper` with everything pre-allocated. The example files
`sample_dpss.rs` and `sample_sine.rs` show simple example calls to the
convenience functions.

The example file `sample_fromfile.rs` compiles into a useful tool, though, for
computing multitaper estimators from columns of a data file (delimited with
space, commmas, or tabs) and writing it into a tab-delimited file whose columns
are the corresponding spectral density estimators. An example call:
````
/path/to/compiled/example /path/to/input /path/to/output NW K PADLEN
````
where `NW` is a float, `K` is an int, and `PADLEN` is the length of the FFT.

I have not made any nice array handling libraries (like `ndarray`) dependencies
to keep the library lean, but it would of course be easy to use the library
functions on `ndarray` slices.

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
