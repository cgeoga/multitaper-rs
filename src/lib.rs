extern crate fourier;
extern crate num_complex;

use fourier::{create_fft_f64, Fft};
use num_complex::Complex;

mod tapers;

// Allocate for the input and output of the FFT and the squared modulus output:
pub fn multitaper_alloc(fftlen: usize) -> (Vec<Complex<f64>>, Vec<f64>) {
    // Allocate the input and output vectors for the FFT:
    let work: Vec<Complex<f64>> = vec![Complex::<f64>::new(0.0, 0.0); fftlen];
    // Allocate the output:
    let out: Vec<f64> = vec![0.0; fftlen];
    (work, out)
}

// A convenience function to compute the tapers, do all the allocation, and then
// call the mostly non-allocating internal function:
pub fn multitaper_dpss(src: &[f64], nw: f64, k: usize, len: usize) -> Vec<f64> {
    // Get the allocated arrays:
    let (mut work, mut out) = multitaper_alloc(len);
    // Get the DPSS tapers:
    let tapers: Vec<f64> = tapers::slepians(src.len(), nw, k);
    // Create the FFT plan:
    let fft = create_fft_f64(len);
    // Compute the estimator:
    multitaper(src, k, len, &tapers, &mut work, &mut out, fft);
    out
}

// A convenience function to compute the tapers, do all the allocation, and then
// call the mostly non-allocating internal function:
pub fn multitaper_sine(src: &[f64], k: usize, len: usize) -> Vec<f64> {
    // Get the allocated arrays:
    let (mut work, mut out) = multitaper_alloc(len);
    // Get the DPSS tapers:
    let tapers: Vec<f64> = tapers::sinetapers(src.len(), k);
    // Create the FFT plan:
    let fft = create_fft_f64(len);
    // Compute the estimator:
    multitaper(src, k, len, &tapers, &mut work, &mut out, fft);
    out
}

// An internal function that is about as non-allocating as I can make it. I
// don't think any code I write is responsible for heap allocations in side this
// function, making it deal for many repeated evaluations.
pub fn multitaper(
    src: &[f64],
    k: usize,
    len: usize,
    tapers: &[f64],
    mut work: &mut [Complex<f64>],
    out: &mut [f64],
    fft: Box<dyn Fft<Real = f64>>,
) {
    // Test that the sizes of all provided pre-allocated things are correct:
    assert_eq!(
        src.len(),
        tapers.len().div_euclid(k),
        "Tapers are not correctly sized."
    );
    assert_eq!(work.len(), len);
    assert_eq!(out.len(), len);

    // Get the mean of the data vector for centering:
    let mean: f64 = src.iter().fold(0.0, |acc, x| acc + x) / (src.len() as f64);

    // For each taper, compute the eigenspectrum and add it to the output:
    for taper in tapers.chunks_exact(src.len()) {
        // Split the FFT input vector to the data part (indices 0..src) and the
        // "pad" part (src..len) to update each component:
        let (data_portion, pad_portion) = work.split_at_mut(src.len());

        // Update the data portion of the FFT input vector:
        let tzip = src.iter().zip(taper).zip(data_portion.iter_mut());
        for ((srcj, taperj), mut taperedj) in tzip {
            taperedj.re = (srcj - mean) * taperj;
            taperedj.im = 0.0;
        }

        // zero out the pad portion of the FFT input vector:
        for mut tj in pad_portion.iter_mut() {
            tj.re = 0.0;
            tj.im = 0.0;
        }

        // Perform the FFT:
        fft.fft_in_place(&mut work);

        // Add each component of the eigenspectrum to the output:
        for (o, e) in out.iter_mut().zip(work.iter()) {
            *o += e.norm_sqr();
        }
    }

    // Divide each element by the number of tapers:
    for o in out.iter_mut() {
        *o /= k as f64;
    }
}
