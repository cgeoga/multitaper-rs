extern crate multitaper;

use std::f64::consts::PI;

fn main() {
    // Compute data that should be a line at frequency 0.2 (on scale [-0.5, 0.5)),
    // with no noise or anything interesting so that we can do some kind of
    // vaguely deterministic testing:
    let data: Vec<f64> = (0..1024)
        .map(|j| (2.0 * PI * 0.2 * (j as f64)).cos())
        .collect();

    // Compute the multitaper estimator.
    let spec: Vec<f64> = multitaper::multitaper_dpss(&data, 4.0, 7, 2048);

    // Check one element (randomly chosen for now... I recognize that this is
    // not a great test).
    assert!((spec[410] - 35.11936652835126).abs() < 1e-10);
}
